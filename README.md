# Compiling LALSuite using ENIGMA

This script tries to build pyCBC and lalsuite on the camputs cluster and Blue Waters

To run this script, copy it where you would like a pyCBC directory with pyCBC to be created.

## Module environment

Then make sure you have the default modules loaded, ie module list must show: 

### UIUC Campus cluster
```
Currently Loaded Modulefiles:
  1) slurm/.20.02.3   2) env/ncsa         3) env/taub         4) vim/8.1
```

### Blue Waters
```
Currently Loaded Modulefiles:
  1) modules/3.2.10.4                      17) PrgEnv-cray/5.2.82
  2) eswrap/1.3.3-1.020200.1280.0          18) cray-mpich/7.7.4
  3) cce/8.7.7                             19) craype-interlagos
  4) craype-network-gemini                 20) torque/6.0.4
  5) craype/2.5.16                         21) moab/9.1.2-sles11
  6) cray-libsci/18.12.1                   22) openssh/7.5p1
  7) udreg/2.3.2-1.0502.10518.2.17.gem     23) xalt/0.7.6.local
  8) ugni/6.0-1.0502.10863.8.28.gem        24) scripts
  9) pmi/5.0.14                            25) OpenSSL/1.0.2m
 10) dmapp/7.0.1-1.0502.11080.8.74.gem     26) cURL/7.59.0
 11) gni-headers/4.0-1.0502.10859.7.8.gem  27) git/2.17.0
 12) xpmem/0.1-2.0502.64982.5.3.gem        28) wget/1.19.4
 13) dvs/2.5_0.9.0-1.0502.2188.1.113.gem   29) user-paths
 14) alps/5.2.4-2.0502.9774.31.12.gem      30) gnuplot/5.0.5
 15) rca/1.0.0-2.0502.60530.1.63.gem       31) darshan/3.1.3
 16) atp/2.0.4
```

## Compiling

Then run the script:

```
./install_pyCBC.sh
```

The script *should* let you allow to re-run it if things failed but you
may have to remove some failed output directory. Please have a look and
look for `if ! [-d ... ] ; then` like lines to see how it tries to skip
steps already done

## Test run

Before running you must load the require modules once more

### UIUC Campus cluster
```
module load python/3
unset PYTHONPATH

module unload openmpi
module unload intel
module load openmpi/3.1.1-gcc-7.2.0
```

### Blue Waters
```
module swap PrgEnv-cray PrgEnv-gnu/5.2.82-gcc.4.9.3
module swap cray-mpich cray-mpich/7.5.0
module swap gcc gcc/5.3.0
module unload OpenSSL
module load bwpy/2.0.4
module load bwpy-mpi
module load cray-hdf5
module load fftw
module load gsl/1.16
# this creates a new shell, bwpy-environ is needed for "activate" to work
exec bwpy-environ

```

### Run

Now you can use pyCBC like so:

```
cd pyCBC # important, LAL will not be found otherwise
source ./bin/activate

python -c 'from pycbc.waveform import *; get_td_waveform(approximant="SEOBNRv3", mass1=10., mass2=10., spin1x=0, spin1y=0, spin1z=0, spin2x=0, spin2y=0, spin2z=0, f_lower=25,   delta_t=1./(4096))'
```
