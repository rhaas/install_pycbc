#!/bin/bash
#PBS -l nodes=4:ppn=10 
#PBS -q secondary 
#PBS -l walltime=2:00:00

#echo commands to stdout
#set -x

cd $PBS_O_WORKDIR

module load python/3
unset PYTHONPATH

module unload openmpi
module unload intel
module load openmpi/3.1.1-gcc-7.2.0

source $HOME/tmp/pyCBC/bin/activate

#NPROCS=`nproc`
#export OMP_NUM_THREADS=1
#
## create a new hostfile allocating OMP_NUM_THREADS threads to each MPI rank
#for n in $(sort -u $PBS_NODEFILE) ; do
#  for t in $(seq 1 $OMP_NUM_THREADS $NPROCS) ; do
#    echo $n
#  done
#done >NODES.$PBS_JOBID
#
#mpirun -n $(wc -l <NODES.$PBS_JOBID) -hostfile NODES.$PBS_JOBID python SEOBNRv3.py 5 100

mpirun -n $(wc -l <$PBS_NODEFILE) python SEOBNRv3.py 5 100