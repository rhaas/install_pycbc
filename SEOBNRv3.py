#!/usr/bin/env python

from pycbc.waveform import *
from pycbc.pnutils import *
from numpy import *
from h5py import *
from multiprocessing import Process
import sys
import numpy as np

from mpi4py import MPI
from mpi4py.futures import MPICommExecutor

# Calculate the initial frequency
def freq(M, q, t):
    M = M*1.989*10**30
    return (((8*3.1415926)**(8/3.)/5.)*(6.67408 * 10.**(-11) * (M**2*q/(1+q)**2)**(3/5.)/((M)**(1/5.))/(299792458.**(3)))**(5/3.)*t)**(-3/8.) - 1.7326

def worker(f_init,M,m1,m2,s1z,s2z):
  s1x=0.0
  s1y=0.0
  s2x=0.0
  s2y=0.0
# Generate waveforms with specified parameters
  try:
    hp, hc = get_td_waveform(approximant='SEOBNRv3', mass1=m1, mass2=m2, spin1x=s1x, spin1y=s1y, spin1z=s1z, spin2x=s2x, spin2y=s2y, spin2z=s2z, f_lower=f_init,   delta_t=1./(4096))
  except Exception as e:
    print("get_td_waveform failed for (%s,%s,%s,%s,%s): %s" % (m1,m2,s1z,s2z,f_init,e))
    waves = np.zeros(8192, dtype=np.float32)
    return(waves, 0)
  length = len(hp.data)
# Check if the waveform is longer than 2s. If not pad with zeroes.
  print("%s %s" % (M, length))
  if length < 8192:
    print(f_init,M,m1,m2,s1z,s2z,'bad')
    waves = np.zeros(8192, dtype=np.float32)
    waves[8192-length:] = hp.data[:]
  else:
    waves = np.zeros((8192), dtype=np.float32)
    waves[:] = hp.data[length-8192:length]
  return (waves, length)


def generator(M1, M2):
# M1 is the total mass
  if (M1 in [27., 39., 47., 59., 63., 9.]):
    # gives SEGFAULTS otherwise
    np.random.seed(int(M1+M2))
  else:
    np.random.seed(int(M1))
  for M in arange(M1, M2, 1.0):
    split = 0
    waveforms = []
    parameters = []
    waveforms_test = []
    parameters_test = []
    tasks = []
    for q in arange(1, 5.1, 0.1): 
      for z1 in arange(-0.9, 0.91, 0.1):
        for z2 in arange(-0.9, 0.91, 0.1):
# Calculate the parameters for the waveforms. Only spins in z direction are nonzero
          eta = q / (1. + q)**2
          m1, m2 = mtotal_eta_to_mass1_mass2(M, eta)
        
          f_init = freq(M, q, 2)
          tasks.append([f_init,M,m1,m2,z1,z2])

    
    name=str(M)+'.h5'
    if(os.path.exists('train'+name)):
      continue # already done

    with MPICommExecutor(MPI.COMM_WORLD, root=0) as executor:
      if executor is not None:
        results = executor.starmap(worker, tasks)

    MPI.COMM_WORLD.Barrier()
    if MPI.COMM_WORLD.rank == 0:
      for i,result in enumerate(results):
        print("%d of %d done" % (i, len(tasks)))
        (f_init,M,m1,m2,z1,z2) = tasks[i]
        (waves, length) = result
        waves = np.float32(waves)
        split = np.random.random()
# Split the waveforms to training and test set. The split ratio is about 4 to 1.
        if split > 0.2:
          waveforms.append(waves)
          parameters.append([m1,m2,z1,z2,length])
        else:
          waveforms_test.append(waves)
          parameters_test.append([m1,m2,z1,z2,length])
    
# The output will be saved to hdf5 files.
      name=str(M)+'.h5.tmp'
      with File('train'+name, 'w') as f:
        f.create_dataset('h', data=np.asarray(waveforms), dtype=np.float32)
        f.create_dataset('params', data=np.asarray(parameters), dtype=np.float32)
    
      with File('test'+name, 'w') as f:
        f.create_dataset('h', data=np.asarray(waveforms_test), dtype=np.float32)
        f.create_dataset('params', data=np.asarray(parameters_test), dtype=np.float32)

      os.rename('train'+name, 'train'+name.replace(".tmp",""))
      os.rename('test'+name, 'test'+name.replace(".tmp",""))

if __name__ == '__main__':
  arg1 = float(sys.argv[1])
  arg2 = float(sys.argv[2])
  generator(arg1, arg2)