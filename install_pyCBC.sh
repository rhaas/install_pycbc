#!/bin/bash

set -e

# parallel build jobs in make
NJOBS=3

# which version of LAL to use? Public or NCSA specific one (contains ENIGMA)
LALSUITE_URL="https://git.ligo.org/lscsoft/lalsuite"
LALSUITE_BRANCH="release-20211210"

# uncomment these to use ENIGMA
#LALSUITE_URL="https://git.ncsa.illinois.edu/rhaas/enigma_attachment.git"
#LALSUITE_BRANCH="rhaas/pnorder"

if [[ x$HOSTNAME == xh2ologin[1-4] ]] ; then
  # need to be inside of bwpy-environ to see all installed software
  if ! [ -d /mnt/bwpy/single ] ; then
    module swap PrgEnv-cray PrgEnv-gnu/5.2.82-gcc.4.9.3
    module unload cray-mpich # no MPI code is to be built
    module swap gcc gcc/5.3.0
    module unload cmake
    module unload xalt
    module unload darshan
    module unload OpenSSL
    module unload git
    module unload cURL
    module unload gnuplot
    module load wget
    module load bwpy/2.0.4
    module load cray-hdf5
    module load fftw
    module load gsl/1.16
    exec bwpy-environ -- /bin/bash -$- -- "$0"
  fi
  PYTHON_EXE=python3.6
  export CPATH="${CPATH}:${BWPY_INCLUDE_PATH}"
  export LIBRARY_PATH="${LIBRARY_PATH}:${BWPY_LIBRARY_PATH}"
  export LDFLAGS="${LDFLAGS} -Wl,--rpath,${BWPY_LIBRARY_PATH}"
  export CPATH="${CPATH}:${BWPY_DIR}/usr/include"
  export LIBRARY_PATH="${LIBRARY_PATH}:${BWPY_DIR}/usr/lib"
  export LDFLAGS="${LDFLAGS} -Wl,--rpath,${BWPY_DIR}/usr/lib"
  export PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:${BWPY_DIR}/usr/share/pkgconfig"
  export CRAYPE_LINK_TYPE=dynamic
  export CRAY_ADD_RPATH=yes
  export CXX=CC
  export CC=cc
  export F90=ftn
  export F77=ftn
  SYSTEM_PACKAGES=--system-site-packages
  HAVE_SWIG=true
  SWIG_DIR=/mnt/bwpy/single/usr
  SWIG_INC=/mnt/bwpy/single/usr/include
  SWIG_BIN=/mnt/bwpy/single/usr/bin
  HAVE_HDF5=true
  # HDF5_DIR= set by module
  HAVE_FFTW=true
  # FFTW_DIR
  # FFTW_INC
  HAVE_OPENSSL=true
elif [[ x$HOSTNAME == xgolubh[1-9].campuscluster.illinois.edu ]] ; then
  module load python/3
  module load gcc
  module load openmpi/3.1.1-gcc-7.2.0
  module load git
  module load gsl/2.5
  export PKG_CONFIG_PATH=/usr/local/src/gsl/2.5/lib/pkgconfig:$PKG_CONFIG_PATH
  PYTHON_EXE=python3.7
  SYSTEM_PACKAGES=
  HAVE_SWIG=false
  HAVE_HDF5=false
  HAVE_FFTW=false
  HAVE_OPENSSL=true
  # this interferes badly with virtualenv
  unset PYTHONPATH
else
  echo "Unknonw cluster. Proceed at your own risk"
  sleep 30
fi

mkdir -p  pyCBC pyCBC/src pyCBC/opt
cd pyCBC/

if ! $PYTHON_EXE -c 'import virtualenv' ; then
  export PYTHONUSERBASE=$PWD
  if ! $PYTHON_EXE -c 'import virtualenv' ; then
    $PYTHON_EXE -m pip install --user virtualenv
  fi
fi

# let's (maybe) forget about system packages, they seem to cause lots of trouble
if ! [ -r $PWD/bin/activate ] ; then
  $PYTHON_EXE -m virtualenv $SYSTEM_PACKAGES -p `which $PYTHON_EXE` $PWD
fi
source bin/activate

# clone this early to that errors in access are caught early
cd $VIRTUAL_ENV/src
if ! [ -d lalsuite ] ; then
  git clone --depth 1 -b $LALSUITE_BRANCH $LALSUITE_URL lalsuite
fi

# download this early do that errors by sourceforge are caugt early
if ! $HAVE_SWIG ; then
  cd ${VIRTUAL_ENV}/src
  if python --version | grep -q "^Python 3[.]" ; then
    SWIG_VERSION=3.0.10
  else
    SWIG_VERSION=2.0.12
  fi
  if ! [ -d swig-$SWIG_VERSION ] ; then
    wget --tries=10 -L https://sourceforge.net/projects/swig/files/swig/swig-$SWIG_VERSION/swig-$SWIG_VERSION.tar.gz
    tar xf swig-$SWIG_VERSION.tar.gz
  fi
  if ! [ -d ${VIRTUAL_ENV}/opt/swig-$SWIG_VERSION ] ; then
    cd swig-$SWIG_VERSION
    ./configure --prefix=${VIRTUAL_ENV}/opt/swig-$SWIG_VERSION
    make -j$NJOBS
    make install || true
  fi
  SWIG_DIR=$VIRTUAL_ENV/opt/swig-$SWIG_VERSION/lib
  SWIG_INC=$VIRTUAL_ENV/opt/swig-$SWIG_VERSION/include
  SWIG_BIN=$VIRTUAL_ENV/opt/swig-$SWIG_VERSION/bin
fi

cd $VIRTUAL_ENV
mkdir -p opt
cd $VIRTUAL_ENV/opt

mkdir -p lalsuite-extra-enigma/data/lalsimulation/
cd lalsuite-extra-enigma/data/lalsimulation/

if ! [ -r ENIGMA.h5 ] ; then
  wget https://ekohaes8.ncsa.illinois.edu/~rhaas/ENIGMA_Attachment/ENIGMA.h5
fi

if ! [ -r SEOBNRv4ROM_v2.0.hdf5 ] ; then
  wget https://git.ligo.org/lscsoft/lalsuite-extra/-/raw/master/data/lalsimulation/SEOBNRv4ROM_v2.0.hdf5
fi

cd $VIRTUAL_ENV

if ! python -c 'import sys,nose; sys.exit(not tuple(map(int, nose.__version__.split("."))) >= (1,0,0))'; then
  python -m pip install "nose>=1.0.0"
fi

if ! python -c 'import sys,numpy; sys.exit(not tuple(map(int, numpy.__version__.split("."))) >= (1,16,0))'; then
  python -m pip install "numpy>=1.16.0,<=1.17.0"
fi
if ! python -c 'import sys,cython; sys.exit(not tuple(map(int, cython.__version__.split("."))) >= (0,29,0))'; then
  python -m pip install 'Cython>=0.29'
fi
if ! python -c "import unittest"; then
  python -m pip install unittest2
fi
if ! python -c "import decorator"; then
  python -m pip install decorator
fi
if python --version | grep -q "^Python 2[.]" ; then
  if ! python -c "import cjson"; then
    python -m pip install python-cjson
  fi
fi
if ! python -c 'import sys,astropy; sys.exit(not tuple(map(int, astropy.__version__.split("."))) >= (2,0,3))'; then
  python -m pip install "astropy>=2.0.3,<3.0.0"
fi


if ! $HAVE_HDF5 ; then
  cd $VIRTUAL_ENV/src
  if ! [ -d hdf5-1.8.12 ] ; then
    curl https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.8/hdf5-1.8.12/src/hdf5-1.8.12.tar.gz >hdf5-1.8.12.tar.gz
    tar xf hdf5-1.8.12.tar.gz
  fi
  cd hdf5-1.8.12/
  if ! [ -d $VIRTUAL_ENV/opt/hdf5-1.8.12 ] ; then
    ./configure --prefix=$VIRTUAL_ENV/opt/hdf5-1.8.12
    make -j$NJOBS install
  fi
  if ! python -c 'import h5py'; then
    HDF5_DIR=$VIRTUAL_ENV/opt/hdf5-1.8.12 python -m pip install h5py
  fi
fi

cd $VIRTUAL_ENV/src
if ! $HAVE_FFTW ; then
  if ! [ -d fftw-3.3.6-pl2 ] ; then
    curl http://www.fftw.org/fftw-3.3.6-pl2.tar.gz >fftw-3.3.6-pl2.tar.gz
    tar xf fftw-3.3.6-pl2.tar.gz
  fi
  cd fftw-3.3.6-pl2
  if ! [ -d $VIRTUAL_ENV/opt/fftw-3.3.6-pl2 ] ; then
    ./configure --enable-shared --enable-float --prefix=$VIRTUAL_ENV/opt/fftw-3.3.6-pl2
    make -j$NJOBS install
    ./configure --enable-shared --prefix=$VIRTUAL_ENV/opt/fftw-3.3.6-pl2
    make -j$NJOBS install
  fi

  FFTW_DIR=$VIRTUAL_ENV/opt/fftw-3.3.6-pl2/lib
  FFTW_INC=$VIRTUAL_ENV/opt/fftw-3.3.6-pl2/include
fi

if ! $HAVE_OPENSSL ; then
  cd ${VIRTUAL_ENV}/src
  if ! [ -d openssl-1.0.1u/ ] ; then
    wget https://www.openssl.org/source/old/1.0.1/openssl-1.0.1u.tar.gz
    tar xf openssl-1.0.1u.tar.gz
  fi
  cd openssl-1.0.1u/
  if ! [ -d ${VIRTUAL_ENV}/opt/openssl-1.0.1u ] ; then
    ./config shared --prefix=${VIRTUAL_ENV}/opt/openssl-1.0.1u
    make -j$NJOBS
    make install || true
  fi
  # --global-option="--openssl=${VIRTUAL_ENV}/opt/openssl-1.0.1u"
  SWIG_FEATURES+=" -I${VIRTUAL_ENV}/opt/openssl-1.0.1u/include/openssl"
  export CFLAGS="-I${VIRTUAL_ENV}/opt/openssl-1.0.1u/include/openssl -L${VIRTUAL_ENV}/opt/openssl-1.0.1u/lib:$CFLAGS"
fi

cd ${VIRTUAL_ENV}/src
export SWIG_FEATURES="-cpperraswarn -includeall"
if ! python -c 'import typing'; then
  python -m pip install typing
fi
if ! python -c 'import parameterized'; then
  python -m pip install parameterized
fi
if ! python -c 'import M2Crypto'; then
  python -m pip install --global-option=build_ext M2Crypto
fi
# -includeall does not work for lal's swig bindings
unset SWIG_FEATURES

# libframe may not be required since I disable all its users
cd ${VIRTUAL_ENV}/src
if ! [ -d libframe-8.30/ ] ; then
  wget http://lappweb.in2p3.fr/virgo/FrameL/libframe-8.30.tar.gz
  tar xf libframe-8.30.tar.gz 
fi
if ! [ -d $VIRTUAL_ENV/opt/libframe-8.30 ] ; then
  cd libframe-8.30/
  ./configure --prefix $VIRTUAL_ENV/opt/libframe-8.30
  make -j$NJOBS install
fi

# metaio may not be required anymore since I disable all its users
cd ${VIRTUAL_ENV}/src
if ! [ -d metaio-8.4.0 ] ; then
  wget http://software.ligo.org/lscsoft/source/metaio-8.4.0.tar.gz
  tar xf metaio-8.4.0.tar.gz 
fi
cd metaio-8.4.0/
if ! [ -d $VIRTUAL_ENV/opt/metaio-8.4.0/ ] ; then
  ./configure --prefix $VIRTUAL_ENV/opt/metaio-8.4.0/
  make -j$NJOBS install
fi

cd ${VIRTUAL_ENV}/src
# already checked out at the beginning
cd lalsuite
if ! [ -d ${VIRTUAL_ENV}/opt/lalsuite ] ; then
  ./00boot
  export LDFLAGS="-lpthread -Wl,--rpath,$SWIG_DIR -L$SWIG_DIR -Wl,--rpath,$FFTW_DIR -L$FFTW_DIR -L${VIRTUAL_ENV}/opt/libframe-8.30/lib -L${VIRTUAL_ENV}/opt/metaio-8.4.0/lib" 
  export CPATH="$SWIG_INC:$FFTW_INC:${VIRTUAL_ENV}/opt/libframe-8.30/include:${VIRTUAL_ENV}/opt/metaio-8.4.0/include:$CPATH"
  export CFLAGS"=-Wno-error -O2"
  export PATH=$SWIG_BIN:$PATH
  PYTHON=$PYTHON_EXE ./configure --prefix=${VIRTUAL_ENV}/opt/lalsuite --enable-swig-python --disable-lalstochastic --disable-lalxml --disable-lalinference --disable-laldetchar --disable-lalapps --disable-lalframe --disable-lalinspiral
  make -j$NJOBS install
  unset CFLAGS

  cd  ${VIRTUAL_ENV}/src
  echo 'source ${VIRTUAL_ENV}/opt/lalsuite/etc/lalsuite-user-env.sh' >> ${VIRTUAL_ENV}/bin/activate
  echo "export LAL_DATA_PATH=${VIRTUAL_ENV}/opt/lalsuite-extra-enigma/data/lalsimulation" >> ${VIRTUAL_ENV}/bin/activate
  SAVED_VIRTUAL_ENV=$VIRTUAL_ENV
  deactivate
  source $SAVED_VIRTUAL_ENV/bin/activate
fi

cd ${VIRTUAL_ENV}/src
# cannot use pip due to faulty pyproject file
if ! [ -d pycbc-1.16.5 ] ; then
  wget -O pycbc-1.16.5.zip https://github.com/gwastro/pycbc/archive/v1.16.5.zip 
  unzip pycbc-1.16.5.zip
fi
cd pycbc-1.16.5
# fix pyproject.m file ()
echo 'build-backend="setuptools.build_meta"' >>pyproject.toml

python ./setup.py install

# install mpi4py after pyCBC, otherwise pyCBC tries to run MPI code which fails
if [[ x$HOSTNAME == xh2ologin[1-4] ]] ; then
  module load bwpy-mpi
fi

if ! python -c 'import mpi4py'; then
  python -m pip install mpi4py
fi
